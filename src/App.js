import React, { useEffect, useState } from "react";
import Main from "./components/main/Main";
import graphQlToRest from "../src/components/data/fetchData";

function App() {
  const [cardData, setCardData] = useState({});
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    graphQlToRest()
      .then((cardData) =>
        setCardData(cardData.data.getHomePageArticle.aResults)
      )
      .then(() => setLoading(false));
  }, []);

  return (
    <div className="app">
      <div className="container">
        {loading ? <h1>Loading...</h1> : <Main cardData={cardData} />}
      </div>
    </div>
  );
}

export default App;
