import React from "react";
import PropTypes from "prop-types";

function Nbig({ article }) {
  return (
    <div className="bigCard">
      <img src={article.oImg.sUrl} alt="BigCardImg" />
      <h3>{article.sTitle}</h3>
      <p>{article.sDescription}</p>

      <div className="date-container">
        <p className="duration">{article.nDuration} Min</p>
      </div>
    </div>
  );
}

Nbig.propTypes = {
  article: PropTypes.object,
};
export default Nbig;
