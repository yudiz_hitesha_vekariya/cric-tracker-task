import React from "react";
import PropTypes from "prop-types";
import Ngrid from "../parts/Ngrid";
import Nsmall from "../parts/Nsmall";
import Nbig from "./../parts/Nbig";

function Main({ cardData }) {
  console.log(cardData);
  return (
    <div className="home">
      {cardData.map((card, index) => (
        <div className="card" key={index}>
          <h1>{card.sName}</h1>
          <hr className="hr" />
          {cardData.map((article, index) => (
            <>
              <div key={index}>
                {article.sType == "nBig" && <Nbig article={article} />}
                {article.sType == "nSmall" && <Nsmall article={article} />}
                {article.sType == "nGrid" && <Ngrid article={article} />}
              </div>
            </>
          ))}
          <button>More From {card.sName}</button>
        </div>
      ))}
    </div>
  );
}

Main.propTypes = {
  cardData: PropTypes.array,
};

export default Main;
